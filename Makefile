.PHONY: run clean test build

run:
	flutter run -d web-server --web-hostname=0.0.0.0 --web-port=8080

clean:
	flutter clean

test:
	flutter test

build:
	flutter build web --web-renderer=html --no-tree-shake-icons --release