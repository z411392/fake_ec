import 'package:flutter/material.dart' show ValueNotifier;
import 'package:shared_preferences/shared_preferences.dart' show SharedPreferences;
import 'dart:async' show FutureOr;
import 'dart:io' show Platform;
import 'package:uuid/uuid.dart' show Uuid;
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:device_info_plus/device_info_plus.dart' show DeviceInfoPlugin;

String get uuid => const Uuid().v4().toString();

final deviceId = ValueNotifier<String?>(null);
FutureOr<String?> _getDeviceId() async {
  if (kIsWeb) return uuid;
  final deviceInfo = DeviceInfoPlugin();
  if (Platform.isAndroid) return (await deviceInfo.androidInfo).id;
  if (Platform.isAndroid) return (await deviceInfo.iosInfo).identifierForVendor ?? '';
  return null;
}

Future<void> initializeDeviceInfo(SharedPreferences? sharedPreferences) async {
  var value = sharedPreferences?.getString('deviceId');
  if (value == null) {
    value = (await _getDeviceId()) ?? uuid;
    await sharedPreferences?.setString('deviceId', value);
  }
  deviceId.value = value;
}
