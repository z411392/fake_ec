// ignore_for_file: curly_braces_in_flow_control_structures

import 'dart:async' show StreamController, Stream;
import 'package:flutter/material.dart' show ValueNotifier;
import 'package:fake_ec/models/credentials.dart' show CredentialsModel;
import 'package:shared_preferences/shared_preferences.dart' show SharedPreferences;
import 'dart:convert' show jsonDecode, jsonEncode;
import 'package:shared_preferences/shared_preferences.dart';

final _topic = StreamController<CredentialsModel?>.broadcast();
Stream<CredentialsModel?> get onAuthStateChanged => _topic.stream;
get triggerAuthStateChanged => _topic.sink.add;

initializeAuthState(SharedPreferences? sharedPreferences) {
  onAuthStateChanged.listen((CredentialsModel? value) {
    $credentials.value = value;
    sharedPreferences?.setString('credentials', jsonEncode(value));
  });
  final credentials = sharedPreferences?.getString('credentials');
  if (credentials == null) return;
  triggerAuthStateChanged(CredentialsModel.fromJson(jsonDecode(credentials)));
}

final $credentials = ValueNotifier<CredentialsModel?>(null);
