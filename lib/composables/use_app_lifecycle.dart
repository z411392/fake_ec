import 'dart:async' show StreamController;
import 'package:flutter/material.dart' show ValueNotifier, AppLifecycleState;
import 'package:shared_preferences/shared_preferences.dart' show SharedPreferences;

final _topic = StreamController<AppLifecycleState>.broadcast();
get triggerAppLifecycleStateChanged => _topic.sink.add;

initializeAppState(SharedPreferences? _) => _topic.stream.listen((AppLifecycleState value) => $appLifecycleState.value = value);

final $appLifecycleState = ValueNotifier<AppLifecycleState?>(null);
