import 'package:flutter/material.dart';
import 'package:fake_ec/utils/startup.dart' show startup;
import 'package:fake_ec/composables/use_app_lifecycle.dart' show triggerAppLifecycleStateChanged;

class _StartupPageState extends State<StartupPage> {
  late final AppLifecycleListener _appLifecycleListener;
  @override
  void initState() {
    super.initState();
    _appLifecycleListener = AppLifecycleListener(onStateChange: (AppLifecycleState state) => triggerAppLifecycleStateChanged(state));
  }

  @override
  void dispose() {
    _appLifecycleListener.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: startup(),
      builder: (context, snapshot) => switch (snapshot.connectionState) {
        ConnectionState.done => Builder(
            builder: (context) {
              if (snapshot.hasError) return Center(child: Text(snapshot.error!.toString()));
              return widget.child;
            },
          ),
        _ => const Center(child: CircularProgressIndicator()),
      },
    );
  }
}

class StartupPage extends StatefulWidget {
  final Widget child;
  const StartupPage({super.key, required this.child});

  @override
  State<StatefulWidget> createState() => _StartupPageState();
}
