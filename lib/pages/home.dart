import 'package:flutter/material.dart';
import 'package:fake_ec/router.dart' show Routes;
import 'package:qlevar_router/qlevar_router.dart' show QR;
import 'package:fake_ec/mixins/response_design_helper.dart' show ResponsiveDesignHelper;

class HomePage extends StatelessWidget with ResponsiveDesignHelper {
  final Widget child;
  const HomePage({super.key, required this.child});

  _showTopic(BuildContext context, int index) {
    final navigator = QR.navigatorOf(Routes.home.name);
    return switch (index) {
      0 => navigator.replaceAllWithName(Routes.posts.name),
      1 => navigator.replaceAllWithName(Routes.todos.name),
      3 => navigator.replaceAllWithName(Routes.cart.name),
      4 => navigator.replaceAllWithName(Routes.my.name),
      _ => navigator.replaceAllWithName(Routes.categories.name),
    };
  }

  @override
  Widget build(BuildContext context) {
    final width = maxWidth(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: width,
          child: Scaffold(
            body: child,
            bottomNavigationBar: BottomNavigationBar(
              showUnselectedLabels: true,
              onTap: (int index) => _showTopic(context, index),
              items: const [
                BottomNavigationBarItem(
                  icon: Icon(Icons.message_outlined),
                  label: '最新消息',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.list_alt_outlined),
                  label: '待辦事項',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.compass_calibration_outlined),
                  label: '探索',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.shopping_cart_checkout_outlined),
                  label: '購物車',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.account_box_outlined),
                  label: '我的',
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
