// ignore_for_file: curly_braces_in_flow_control_structures
import 'package:flutter/material.dart';
import 'package:fake_ec/router.dart' show Routes;
import 'package:qlevar_router/qlevar_router.dart' show QR;
import 'package:fake_ec/components/product_category_tabs.dart' show ProductCategoryTabs;

class CategoriesPage extends StatelessWidget {
  final Future<List<String>> future;
  final Widget child;
  const CategoriesPage({super.key, required this.future, required this.child});

  _showCategory(String categoryName) =>
      QR.navigatorOf(Routes.categories.name).replaceAllWithName(Routes.category.name, params: {'categoryName': categoryName});

  @override
  Widget build(BuildContext context) {
    final categoryNames = <String>[];
    return FutureBuilder(
      future: future,
      builder: (context, snapshot) {
        categoryNames.addAll(snapshot.data ?? []);
        return Scaffold(
          appBar: AppBar(
            centerTitle: false,
            title: const Text('探索'),
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(kToolbarHeight),
              child: switch (snapshot.connectionState) {
                ConnectionState.done => Builder(
                    builder: (context) {
                      if (snapshot.hasError) return Center(child: Text(snapshot.error!.toString()));
                      return ProductCategoryTabs(
                        categoryNames: categoryNames,
                        onTap: (categoryName) => _showCategory(categoryName),
                        onMount: (categoryName) => _showCategory(categoryName),
                      );
                    },
                  ),
                _ => const SizedBox(),
              },
            ),
          ),
          body: switch (snapshot.connectionState) {
            ConnectionState.done => child,
            _ => const Center(child: CircularProgressIndicator()),
          },
        );
      },
    );
  }
}
