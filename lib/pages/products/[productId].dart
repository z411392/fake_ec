// ignore_for_file: file_names
import 'package:flutter/material.dart';
import 'package:fake_ec/utils/dummy_json.dart' show productOfId;
import 'package:qlevar_router/qlevar_router.dart' show QR, QRNavigation;
import 'package:fake_ec/components/product_detail.dart' show ProductDetail;
import 'package:fake_ec/mixins/response_design_helper.dart' show ResponsiveDesignHelper;

class ProductPage extends StatelessWidget with ResponsiveDesignHelper {
  const ProductPage({super.key});

  _backToPreviousPage(BuildContext context) => QR.back();

  _productId(BuildContext context) => QR.params['productId']!.asInt;

  @override
  Widget build(BuildContext context) {
    final width = maxWidth(context);
    final productId = _productId(context);
    final future = productOfId(productId);
    return Center(
      child: SizedBox(
        width: width,
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () => _backToPreviousPage(context),
            ),
            title: FutureBuilder(
              future: future,
              builder: (context, snapshot) {
                if (snapshot.hasData) return Text(snapshot.data!.title);
                return const SizedBox();
              },
            ),
          ),
          body: FutureBuilder(
            future: future,
            builder: (context, snapshot) => switch (snapshot.connectionState) {
              ConnectionState.done => Builder(
                  builder: (context) {
                    if (snapshot.hasError) return Center(child: Text(snapshot.error!.toString()));
                    final product = snapshot.data!;
                    return ProductDetail(product: product);
                  },
                ),
              _ => const Center(child: CircularProgressIndicator()),
            },
          ),
        ),
      ),
    );
  }
}
