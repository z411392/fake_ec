import 'package:fake_ec/utils/dummy_json.dart' show signIn;
import 'package:flutter/material.dart';
import 'package:qlevar_router/qlevar_router.dart' show QR, QRNavigation;
import 'package:fake_ec/router.dart' show Routes;
import 'package:fake_ec/mixins/response_design_helper.dart' show ResponsiveDesignHelper;

class SignInPage extends StatelessWidget with ResponsiveDesignHelper {
  final _$isValid = ValueNotifier(false); // _$isValid 初始化時為 false，在 mounted、changed 時會檢查並更新這個值
  final _usernameState = GlobalKey<FormFieldState>();
  final _passwordState = GlobalKey<FormFieldState>();

  SignInPage({super.key});

  _validate(BuildContext context) => _$isValid.value = _usernameState.currentState!.isValid && _passwordState.currentState!.isValid;

  final _$isLoading = ValueNotifier(false);

  _submit(BuildContext context) {
    final username = _usernameState.currentState!.value;
    final password = _passwordState.currentState!.value;
    _$isLoading.value = true;
    signIn(username, password).then((_) => QR.toName(Routes.categories.name)).whenComplete(() => _$isLoading.value = false);
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => _validate(context));
    final width = maxWidth(context);
    return Scaffold(
      appBar: AppBar(title: const Text('登入畫面')),
      body: Center(
        child: Container(
          width: width,
          padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 16),
          child: Card(
            clipBehavior: Clip.hardEdge,
            child: Form(
              onChanged: () => _validate(context),
              child: IntrinsicHeight(
                child: Column(
                  children: [
                    TextFormField(
                      key: _usernameState,
                      initialValue: r'atuny0',
                      decoration: const InputDecoration(
                        labelText: '使用者名稱',
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      autocorrect: false,
                      validator: (String? value) {
                        if (value == null || value == '') return '必須填寫使用者名稱';
                        if (value.length > 15) return '使用者名稱過長';
                        return null;
                      },
                    ),
                    TextFormField(
                      key: _passwordState,
                      initialValue: r'9uQFF1Lh',
                      decoration: const InputDecoration(
                        labelText: '密碼',
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      autocorrect: false,
                      obscureText: true,
                      validator: (String? value) {
                        if (value == null || value == '') return '必須填寫使用者名稱';
                        if (value.length > 15) return '使用者名稱過長';
                        return null;
                      },
                    ),
                    SizedBox(
                      width: width,
                      child: ListenableBuilder(
                        listenable: Listenable.merge([_$isValid, _$isLoading]),
                        builder: (context, _) {
                          final submittable = _$isValid.value && !_$isLoading.value;
                          final isLoading = _$isLoading.value;
                          return FilledButton(
                            onPressed: submittable ? () => _submit(context) : null,
                            child: isLoading ? const CircularProgressIndicator() : const Text('登入'),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
