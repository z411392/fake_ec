// ignore_for_file: file_names, curly_braces_in_flow_control_structures, must_be_immutable
import 'package:flutter/material.dart';
import 'package:fake_ec/utils/dummy_json.dart' show productsOfCategory;
import 'package:fake_ec/models/product_summary.dart' show ProductSummaryModel;
import 'package:visibility_detector/visibility_detector.dart' show VisibilityDetector;
import 'package:async_queue/async_queue.dart' show AsyncQueue;
import 'package:fake_ec/components/product_summary.dart' show ProductSummary;
import 'package:qlevar_router/qlevar_router.dart' show QR, QRNavigation;
import 'package:fake_ec/router.dart' show Routes;

class CategoryPage extends StatelessWidget {
  final AsyncQueue _queue = AsyncQueue.autoStart();
  final String categoryName;
  CategoryPage({super.key, required this.categoryName});
  final _$lastUpdatedAt = ValueNotifier(0);
  final List<ProductSummaryModel?> _products = [null];
  int _page = 1;

  _fetchData() async {
    final pagedResult = await productsOfCategory(categoryName, _page);
    if (pagedResult.data.isEmpty) return;
    _products.addAll(pagedResult.data);
    _page += 1;
    _$lastUpdatedAt.value = DateTime.now().second;
  }

  _onVisibilityChanged() => _queue.addJob((__) => _fetchData());
  _showProductDetailModel(int productId) => QR.toName(Routes.product.name, params: {'productId': productId});
  _buildListViewItem(BuildContext context, int index, int lastUpdatedAt) {
    if (_products.length <= index) return null;
    final product = _products[index];
    if (product == null)
      return VisibilityDetector(
        key: ObjectKey((categoryName: categoryName, lastUpdatedAt: lastUpdatedAt)),
        onVisibilityChanged: (_) {
          if (lastUpdatedAt < _$lastUpdatedAt.value) return;
          _onVisibilityChanged();
        },
        child: const SizedBox(height: 0.01),
      );
    return ProductSummary(
      product: product,
      onTap: (productId) => _showProductDetailModel(productId),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
      child: ListenableBuilder(
        listenable: _$lastUpdatedAt,
        builder: (context, _) {
          return ListView.separated(
            itemCount: _products.length,
            itemBuilder: (context, index) => _buildListViewItem(context, index, _$lastUpdatedAt.value),
            separatorBuilder: (context, index) => const SizedBox(height: 16),
          );
        },
      ),
    );
  }
}
