// ignore_for_file: file_names

import 'package:flutter/material.dart';

class NoCategoriesFoundPage extends StatelessWidget {
  const NoCategoriesFoundPage({super.key});
  @override
  Widget build(BuildContext context) {
    return const Center(child: Text('啊～沒有類別可以顯示'));
  }
}
