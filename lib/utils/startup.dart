// ignore_for_file: curly_braces_in_flow_control_structures
import 'package:fake_ec/composables/use_device_info.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' show dotenv;
import 'dart:async' show Future;
import 'package:fake_ec/composables/use_app_lifecycle.dart' show initializeAppState;
import 'package:fake_ec/composables/use_auth.dart' show initializeAuthState;
import 'package:logging/logging.dart' show hierarchicalLoggingEnabled;
import 'package:shared_preferences/shared_preferences.dart' show SharedPreferences;
import 'package:get_it/get_it.dart' show GetIt;
import 'package:fake_ec/utils/development.dart' show isTesting;
import 'package:qlevar_router/qlevar_router.dart' show QR;
import 'package:fake_ec/router.dart' show notFoundPage;

Future<void> startup() async {
  hierarchicalLoggingEnabled = true;
  await dotenv.load(fileName: 'assets/dotenv');
  SharedPreferences? sharedPreferences;
  if (!isTesting) {
    sharedPreferences ??= await SharedPreferences.getInstance();
    GetIt.instance.registerSingleton(sharedPreferences);
  }
  QR.settings.enableDebugLog = false;
  QR.settings.notFoundPage = notFoundPage;
  initializeAppState(sharedPreferences);
  initializeAuthState(sharedPreferences);
  await Future.wait([
    initializeDeviceInfo(sharedPreferences),
    GetIt.instance.allReady(),
  ]);
}
