import 'package:logging/logging.dart' show Logger, Level;
import 'dart:developer' show log;
export 'package:logging/logging.dart' show Level;
import 'development.dart' show isTesting;
import 'package:flutter/foundation.dart' show kIsWeb;

Logger createLogger(String name, [Level level = Level.INFO]) => Logger(name)
  ..level = level
  ..onRecord.listen(
    (record) {
      if (isTesting) return print(record);
      if (kIsWeb) return print(record);
      return log(record.message, level: record.level.value, name: record.loggerName);
    },
  );
