import 'package:dio/dio.dart' show Dio, BaseOptions, Headers, ResponseType, Options, DioException;
import 'dart:convert' show jsonDecode;
import 'package:fake_ec/utils/logging.dart' show createLogger, Level;
import 'package:fake_ec/models/product_summary.dart' show ProductSummaryModel;
import 'package:fake_ec/models/product_detail.dart' show ProductDetailModel;
import 'package:fake_ec/models/paged_result.dart' show PagedResult;
import 'package:fake_ec/models/credentials.dart' show CredentialsModel;
import 'package:fake_ec/composables/use_auth.dart' show triggerAuthStateChanged, $credentials;

final _dummyJson = Dio(
  BaseOptions(
    baseUrl: 'https://dummyjson.com',
    contentType: Headers.jsonContentType,
    responseType: ResponseType.plain,
  ),
);

final _logger = createLogger('dummyjson', Level.INFO);

Map<String, String> get _headers => {if ($credentials.value != null) 'Authorization': 'Bearer ${$credentials.value!.token}'};

Future<CredentialsModel> signIn(String username, String password) async {
  const method = 'post';
  const path = '/auth/login';
  final data = {'username': username, 'password': password};
  _logger.info((method: method, path: path, data: data));
  final response = await _dummyJson.post(path, data: data);
  _logger.info(response.data);
  final payload = jsonDecode(response.data);
  final credentials = CredentialsModel.fromJson(payload);
  // await Future.delayed(const Duration(seconds: 10));
  triggerAuthStateChanged(credentials);
  return credentials;
}

Future<List<String>> allCategories() async {
  try {
    const method = 'get';
    const path = '/auth/products/categories';
    _logger.info((method: method, path: path));
    final response = await _dummyJson.get(path, options: Options(headers: _headers));
    _logger.info(response.data);
    if (response.statusCode == 401) triggerAuthStateChanged(null);
    final payload = jsonDecode(response.data);
    final categories = List<String>.from(payload);
    // await Future.delayed(const Duration(seconds: 10));
    return categories;
  } on DioException catch (exception) {
    if (exception.response == null) rethrow;
    if (exception.response!.statusCode == 401) triggerAuthStateChanged(null);
    rethrow;
  }
}

Future<PagedResult<List<ProductSummaryModel>>> productsOfCategory(String categoryName, int page, [int pageSize = 5]) async {
  try {
    const method = 'get';
    final path = '/auth/products/category/$categoryName';
    final offset = (page - 1) * pageSize;
    final queryParameters = {'limit': pageSize, 'skip': offset};
    _logger.info((method: method, path: path, queryParameters: queryParameters));
    final response = await _dummyJson.get(path, queryParameters: queryParameters, options: Options(headers: _headers));
    _logger.info(response.data);
    if (response.statusCode == 401) triggerAuthStateChanged(null);
    final payload = jsonDecode(response.data);
    final products = List.from(payload['products']).map((dynamic data) => ProductSummaryModel.fromJson(data)).toList();
    final pagedResult = PagedResult<List<ProductSummaryModel>>(
        total: payload['total'] as int, skip: payload['skip'] as int, limit: payload['limit'] as int, data: products);
    // await Future.delayed(const Duration(seconds: 10));
    return pagedResult;
  } on DioException catch (exception) {
    if (exception.response == null) rethrow;
    if (exception.response!.statusCode == 401) triggerAuthStateChanged(null);
    rethrow;
  }
}

Future<ProductDetailModel> productOfId(int productId) async {
  try {
    const method = 'get';
    final path = '/auth/products/$productId';
    _logger.info((method: method, path: path));
    final response = await _dummyJson.get(path, options: Options(headers: _headers));
    _logger.info(response.data);
    if (response.statusCode == 401) triggerAuthStateChanged(null);
    final payload = jsonDecode(response.data);
    final product = ProductDetailModel.fromJson(payload);
    // await Future.delayed(const Duration(seconds: 10));
    return product;
  } on DioException catch (exception) {
    if (exception.response == null) rethrow;
    if (exception.response!.statusCode == 401) triggerAuthStateChanged(null);
    rethrow;
  }
}
