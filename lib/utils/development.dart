import 'dart:io' show Platform;
import 'package:flutter/foundation.dart' show kIsWeb;

bool get isTesting {
  if (kIsWeb) return false;
  return Platform.environment.containsKey('FLUTTER_TEST');
}
