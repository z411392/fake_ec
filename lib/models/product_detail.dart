class ProductDetailModel {
  int get id => data['id'];
  String get title => data['title'];
  String get description => data['description'];
  int get price => data['price'];
  double get discountPercentage => data['discountPercentage'];
  double get rating => data['rating'];
  int get stock => data['stock'];
  String get brand => data['brand'];
  String get category => data['category'];
  String get thumbnail => data['thumbnail'];
  List<String> get images => List<String>.from(data['images']); // json 裡的 List 會是 List<dynamic> 寫 Getter 時一定要幫它確立型別
  final Map<String, dynamic> data;
  ProductDetailModel(this.data);
  factory ProductDetailModel.fromJson(Map<String, dynamic> data) => ProductDetailModel(data);
  Map<String, dynamic> toJson() => data;
}
