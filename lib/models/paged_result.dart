// ignore_for_file: file_names

class PagedResult<T> {
  // 分頁的結構通常會一樣 只有資料內容不一樣
  final int total;
  final int skip;
  final int limit;
  final T data;
  const PagedResult({required this.total, required this.skip, required this.limit, required this.data});
}
