import './product_detail.dart' show ProductDetailModel;

class ProductSummaryModel extends ProductDetailModel {
  // 後來想想應該是少的去繼承多的 以及特例用法去繼承通用用法
  ProductSummaryModel(super.data);
  @override
  List<String> get images => throw UnimplementedError();
  factory ProductSummaryModel.fromJson(Map<String, dynamic> data) {
    return ProductSummaryModel(data);
  }
}
