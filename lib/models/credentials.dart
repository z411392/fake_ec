class CredentialsModel {
  // CredentialsModel 跟 User 要用到的資料份量應該不一樣
  int get id => data['id'];
  String get username => data['username'];
  String get email => data['email'];
  String get firstName => data['firstName'];
  String get lastName => data['lastName'];
  String get gender => data['gender'];
  String get image => data['image'];
  String get token => data['token'];

  CredentialsModel._(this.data);
  final Map<String, dynamic> data;
  factory CredentialsModel.fromJson(Map<String, dynamic> data) {
    return CredentialsModel._(data);
  }
  Map<String, dynamic> toJson() {
    return data;
  }
}
