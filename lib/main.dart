import 'package:flutter/material.dart' show WidgetsFlutterBinding, MaterialApp, runApp, SizedBox, ThemeMode;
import 'router.dart' show routerDelegate;
import 'theme.dart' show theme;
import 'package:fake_ec/pages/startup.dart' show StartupPage;
import 'package:qlevar_router/qlevar_router.dart' show QRouteInformationParser;

const title = 'Flutter Demo';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(
    MaterialApp.router(
      title: title,
      debugShowCheckedModeBanner: false,
      theme: theme,
      builder: (context, child) => StartupPage(child: child ?? const SizedBox()), // 在所有頁面開始之前先進到 Startup 畫面作一些初始化工作（這邊的 Startup 跟 Splash 不一樣）
      routeInformationParser: const QRouteInformationParser(),
      routerDelegate: routerDelegate,
      themeMode: ThemeMode.system,
    ),
  );
}
