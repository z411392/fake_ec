import 'package:flutter/material.dart';
import 'package:qlevar_router/qlevar_router.dart' show QR;

class ProductCategoryTabs extends StatelessWidget {
  final List<String> categoryNames;
  final Function(String categoryName) onTap;
  final Function(String categoryName) onMount;
  const ProductCategoryTabs({super.key, required this.categoryNames, required this.onTap, required this.onMount});
  _categoryName(BuildContext context) => QR.params['categoryName']?.toString();
  _categoryIndex(BuildContext context) {
    final categoryName = _categoryName(context);
    return categoryName == null ? -1 : categoryNames.indexWhere((comparedTo) => categoryName == comparedTo);
  }

  _initialIndex(BuildContext context) {
    final categoryIndex = _categoryIndex(context);
    return categoryIndex == -1 ? 0 : categoryIndex;
  }

  _redirecting(BuildContext context) => _categoryName(context) == null && categoryNames.isNotEmpty;

  @override
  Widget build(BuildContext context) {
    final initialIndex = _initialIndex(context);
    final redirecting = _redirecting(context);
    if (redirecting) WidgetsBinding.instance.addPostFrameCallback((_) => onMount(categoryNames.first));
    return DefaultTabController(
      initialIndex: initialIndex,
      length: categoryNames.length,
      child: TabBar(
        isScrollable: true,
        tabs: [for (final categoryName in categoryNames) Tab(text: categoryName)],
        onTap: (int index) => onTap(categoryNames[index]),
      ),
    );
  }
}
