// ignore_for_file: curly_braces_in_flow_control_structures
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart' show RatingBarIndicator;
import 'package:fake_ec/models/product_summary.dart' show ProductSummaryModel;
import 'package:transparent_image/transparent_image.dart' show kTransparentImage;
import 'package:fake_ec/mixins/response_design_helper.dart' show ResponsiveDesignHelper;

class ProductSummary extends StatelessWidget with ResponsiveDesignHelper {
  final ProductSummaryModel product;
  final Function(int id) onTap;

  const ProductSummary({super.key, required this.product, required this.onTap});

  _title(BuildContext context) => Text(
        product.title,
        style: const TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 16,
        ),
      );
  _lines(BuildContext context) => 3;
  _description(BuildContext context) => Text(
        product.description + List<String>.filled(_lines(context) - 1, '\n').join(''),
        maxLines: _lines(context),
      );

  _thumbnail(BuildContext context) => AspectRatio(
        aspectRatio: 1.77,
        child: FadeInImage.memoryNetwork(
          placeholder: kTransparentImage,
          image: product.thumbnail,
          fit: BoxFit.cover,
        ),
      );

  _divider(BuildContext context) => const SizedBox(height: 8);
  _rating(BuildContext context) => RatingBarIndicator(
        rating: product.rating,
        itemSize: 16.0,
        itemBuilder: (context, index) => const Icon(
          Icons.star,
          color: Colors.amber,
        ),
      );
  _brand(BuildContext context) => Text(
        product.brand,
        style: const TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 10,
          height: 2.5,
        ),
      );

  _stock(BuildContext context) => Text(
        'Stock: ${product.stock}',
        style: const TextStyle(
          fontSize: 18,
          height: 1,
        ),
      );
  _offeredDiscount(BuildContext context) => product.discountPercentage > 0;
  _originalPrice(BuildContext context) {
    final offeredDiscount = _offeredDiscount(context);
    return Text(
      '\$${product.price}',
      style: TextStyle(
        fontSize: offeredDiscount ? 18 : 32,
        fontWeight: offeredDiscount ? FontWeight.w100 : FontWeight.w400,
        decoration: offeredDiscount ? TextDecoration.lineThrough : TextDecoration.none,
        height: 1,
      ),
    );
  }

  _discoutedPrice(BuildContext context) => (product.price * (100 - product.discountPercentage) / 100).ceil();

  _price(BuildContext context) {
    final offeredDiscount = _offeredDiscount(context);
    final price = Badge(
      offset: const Offset(-26, -13),
      isLabelVisible: product.discountPercentage > 0 ? true : false,
      label: Text('↓ ${product.discountPercentage}%'),
      child: Text(
        offeredDiscount ? '\$${_discoutedPrice(context)}' : '',
        style: const TextStyle(
          fontSize: 32,
          height: 1,
        ),
      ),
    );
    final isOnXs = onXs(context);
    if (isOnXs) return price;
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          _originalPrice(context),
          const SizedBox(
            width: 8,
          ),
          price,
        ],
      ),
    );
  }

  _info(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _brand(context),
          _title(context),
          _rating(context),
          _divider(context),
          _description(context),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              _stock(context),
              _price(context),
            ],
          ),
        ],
      ),
    );
  }

  Widget _body(BuildContext context) {
    final isOnXs = onXs(context);
    if (isOnXs)
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _thumbnail(context),
          _info(context),
        ],
      );
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Flexible(
            flex: 9,
            child: _thumbnail(context),
          ),
          Flexible(
            flex: 16,
            child: _info(context),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap(product.id),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: _body(context),
      ),
    );
  }
}
