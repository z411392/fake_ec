import 'package:flutter/material.dart';
import 'package:flutter_carousel_widget/flutter_carousel_widget.dart' show FlutterCarousel, CarouselOptions, CircularSlideIndicator;
import 'package:fake_ec/models/product_detail.dart' show ProductDetailModel;
import 'package:fake_ec/mixins/response_design_helper.dart' show ResponsiveDesignHelper;

class ProductDetail extends StatelessWidget with ResponsiveDesignHelper {
  final ProductDetailModel product;
  const ProductDetail({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    final width = maxWidth(context);
    return IntrinsicHeight(
      child: Column(
        children: [
          FlutterCarousel(
            options: CarouselOptions(
              showIndicator: true,
              slideIndicator: const CircularSlideIndicator(indicatorRadius: 3),
              viewportFraction: 1,
              aspectRatio: 1.77,
            ),
            items: product.images.map(
              (dynamic imageURL) {
                return Image.network(
                  imageURL,
                  width: width,
                  fit: BoxFit.cover,
                );
              },
            ).toList(),
          ),
        ],
      ),
    );
  }
}
