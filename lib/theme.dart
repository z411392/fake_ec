import 'package:flutter/material.dart';

get _primaryColor => Colors.amber;

get theme => ThemeData(
      visualDensity: VisualDensity.comfortable,
      useMaterial3: true,
      colorScheme: ColorScheme.fromSwatch(primarySwatch: _primaryColor),
      filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0),
          ),
        ),
      ),
    );
