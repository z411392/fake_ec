import 'pages/home.dart' show HomePage;
import 'pages/auth/sign_in.dart' show SignInPage;
import 'package:fake_ec/composables/use_auth.dart' show $credentials;
import 'pages/posts.dart' show PostsPage;
import 'pages/todos.dart' show TodosPage;
import 'pages/cart.dart' show CartPage;
import 'pages/my.dart' show MyPage;
import 'pages/categories.dart' show CategoriesPage;
import 'pages/categories/error.dart' show NoCategoriesFoundPage;
import 'pages/categories/[categoryName].dart' show CategoryPage;
import 'pages/products/[productId].dart' show ProductPage;
import 'package:fake_ec/utils/dummy_json.dart' show allCategories;
import 'packagE:fake_ec/pages/error.dart' show ErrorPage;
import 'package:qlevar_router/qlevar_router.dart' show QRouterDelegate, QRoute, QMiddlewareBuilder, QR, QNameRedirect;

enum Routes {
  signIn, // 因為 nested routes 寫法的關係 path 存放的不是絕對路徑 於是 Routes 下就不特別宣告取得 path 的邏輯
  home,
  categories,
  category,
  posts,
  todos,
  cart,
  my,
  product;
}

Future<List<String>>? _future;
get initPath => '/_/categories';

get notFoundPage => QRoute(
      path: '/404',
      builder: () => const ErrorPage(),
    );

get authMiddleware => QMiddlewareBuilder(
      redirectGuardNameFunc: (name) async {
        if ($credentials.value == null && name != Routes.signIn.name) return QNameRedirect(name: Routes.signIn.name);
        if ($credentials.value != null && name == Routes.signIn.name) return QNameRedirect(name: Routes.categories.name);
        return null;
      },
    );

get _ensureUserSignedIn =>
    QMiddlewareBuilder(redirectGuardNameFunc: (name) async => $credentials.value == null ? QNameRedirect(name: Routes.signIn.name) : null);

get _ensureUserSignedOut =>
    QMiddlewareBuilder(redirectGuardNameFunc: (name) async => $credentials.value == null ? null : QNameRedirect(name: Routes.categories.name));

QRouterDelegate get routerDelegate => QRouterDelegate(
      [
        QRoute.withChild(
          name: Routes.home.name,
          path: '_', // 如果不用底線開頭區隔開來 會使得 Home 的 QRoute.withChild 內容誤與 /auth/sign-in 共用造成一些問題
          middleware: [_ensureUserSignedIn],
          builderChild: (child) => HomePage(child: child),
          children: [
            QRoute(
              name: Routes.posts.name,
              path: '/products',
              builder: () => const PostsPage(),
            ),
            QRoute(
              name: Routes.todos.name,
              path: '/todos',
              builder: () => const TodosPage(),
            ),
            QRoute.withChild(
              name: Routes.categories.name,
              path: '/categories',
              builderChild: (child) => CategoriesPage(future: _future ??= allCategories(), child: child),
              children: [
                QRoute(
                  name: Routes.category.name,
                  path: '/:categoryName',
                  builder: () => CategoryPage(categoryName: QR.params['categoryName']?.toString() ?? ''),
                ),
                QRoute(
                  path: '',
                  builder: () => const NoCategoriesFoundPage(),
                ),
              ],
            ),
            QRoute(
              name: Routes.cart.name,
              path: '/cart',
              builder: () => const CartPage(),
            ),
            QRoute(
              name: Routes.my.name,
              path: '/my',
              builder: () => const MyPage(),
            ),
            QRoute(
              name: Routes.product.name,
              path: '/product/:productId',
              builder: () => const ProductPage(),
            ),
          ],
        ),
        QRoute(
          name: Routes.signIn.name,
          path: '/auth/sign-in',
          middleware: [_ensureUserSignedOut],
          builder: () => SignInPage(),
        ),
      ],
      initPath: initPath,
    );
