import 'package:flutter/material.dart';
import 'dart:math' show min;

enum Sizes {
  xs,
  sm,
  md,
  lg,
  xl;

  double get maxWidth => switch (this) {
        xs => 576,
        sm => 768,
        lg => 992,
        xl => 1200,
        _ => 1400,
      };
}

mixin ResponsiveDesignHelper {
  screenWidth(BuildContext context) {
    final screen = MediaQuery.of(context).size;
    return screen.width;
  }

  maxWidth(BuildContext context, [Sizes size = Sizes.xs]) => min<double>(screenWidth(context), size.maxWidth);
  onXs(BuildContext context) => screenWidth(context) <= Sizes.xs.maxWidth;
  onSm(BuildContext context) => !onXs(context) && screenWidth(context) <= Sizes.sm.maxWidth;
  onLg(BuildContext context) => !onSm(context) && screenWidth(context) <= Sizes.lg.maxWidth;
  onXl(BuildContext context) => !onLg(context) && screenWidth(context) <= Sizes.xl.maxWidth;
  onXxl(BuildContext context) => screenWidth(context) > Sizes.xl.maxWidth;
}
