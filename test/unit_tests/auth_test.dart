// ignore: unused_import
import 'package:flutter_test/flutter_test.dart';
import 'dart:io' show HttpOverrides;
import 'package:fake_ec/utils/startup.dart' show startup;
import 'package:fake_ec/utils/dummy_json.dart' show signIn;

void main() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = null;
  await startup();
  test(
    '應該要能夠登入並取得 credentials',
    () async {
      final credentials = await signIn(r'atuny0', r'9uQFF1Lh');
      expect(credentials.runtimeType.toString(), 'CredentialsModel');
    },
    skip: true,
  );
}
