import 'package:flutter_test/flutter_test.dart';
import 'dart:io' show HttpOverrides;
import 'package:fake_ec/utils/startup.dart' show startup;
import 'package:fake_ec/utils/dummy_json.dart' show signIn, allCategories, productsOfCategory, productOfId;
import 'package:fake_ec/models/paged_result.dart' show PagedResult;
import 'package:fake_ec/models/product_detail.dart' show ProductDetailModel;
import 'package:fake_ec/models/product_summary.dart' show ProductSummaryModel;

void main() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  HttpOverrides.global = null;
  await startup();
  await signIn(r'atuny0', r'9uQFF1Lh');
  test(
    '應該要能正確取得產品分類',
    () async {
      final categories = await allCategories();
      expect(categories.runtimeType, List<String>);
    },
    skip: true,
  );

  test(
    '應該要能正確取得某一分類的產品清單',
    () async {
      final pagedResult = await productsOfCategory('smartphones', 1);
      expect(pagedResult.runtimeType, PagedResult<List<ProductSummaryModel>>);
    },
    skip: true,
  );

  test(
    '應該要能正確取得某一產品的詳情',
    () async {
      final product = await productOfId(1);
      expect(product.runtimeType, ProductDetailModel);
    },
    skip: true,
  );
}
